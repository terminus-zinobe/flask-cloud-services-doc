#############
Configuration
#############

ENV vars
--------

Define the following environment variables

- (str) Provider. Allowed: class:`[aws, ]`::
  
        CLOUD_SERVICES_PROVIDER=

Aws Config

- (str) Region. Example: 'us-west-2'::
  
        CLOUD_SERVICES_AWS_REGION=

- (str) Credentials: Access Key::

        AWS_ACCESS_KEY_ID=

- (str) Credentials: Secret Key::

        AWS_SECRET_ACCESS_KEY=
