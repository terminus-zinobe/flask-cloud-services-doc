##########
User Guide
##########

Here you will learn how to use the package correctly and you will see useful examples

************
AWS Services
************

Notification
============


Subscribe and Listen Notifications
----------------------------------

Define your route where you want listen the Notification Channel and you decorate it.

    .. code-block:: python

        from flask import Blueprint
        from flask_cloud_services import notifications_listener

        routes_bus = Blueprint('routes_bus', __name__)

        @routes_bus.route('/notification-channel', methods=['GET', 'POST', 'PUT'])
        @notifications_listener
        def aws_sns(data_listener):
            any_topic_arn = "arn:aws:sns:us-west-2:test:test"
            if data_listener.topic_arn == any_topic_arn:
                pass # DO SOMETHING


Publish Events
--------------

If you want publish an event to specific channel,
you just must call the function `publish` send it
`topic_arn` and `message` encoded.

    .. code-block:: python

        from flask_cloud_services import notifications_publisher

        result = notifications_publisher(
            topic_arn="arn:aws:sns:us-west-2:test:test",
            message="{\"data\": \"value\"}"
        )


Storage
=======


Upload File From Filename
-------------------------

Upload your file to an Bucket of AWS.

    .. code-block:: python

        from flask_cloud_services import storage_upload_from_filename

        storage_upload_from_filename(
            filename='/home/user/Desktop/file.pdf',
            bucket_name='my-bucket-name',
            key='file.pdf',
            extra_args={'ACL': 'public-read'} # Default {}
            *args,
            **kwargs
        )

    .. note:: Params:

        .. code-block:: python

            filename: File path
            bucket_name (str): Bucket name
            key (str): File name
            extra_args (dict): Extra args of S3 Service. Default {}
            additional args and kwargs


Upload File From Object
-----------------------

Upload your file to an Bucket of AWS.

    .. code-block:: python

        import tempfile
        from flask_cloud_services import storage_upload_from_file_obj

        temp_file = tempfile.TemporaryFile()
        temp_file.write(b"a text")
        temp_file.seek(0)

        storage_upload_from_file_obj(
            file_obj=temp_file,
            bucket_name='my-bucket-name',
            key='file.pdf',
            extra_args={'ACL': 'public-read'} # Default {}
            *args,
            **kwargs
        )

    .. note:: Params:

        .. code-block:: python

            file_obj: File object
            bucket_name (str): Bucket name
            key (str): File name
            extra_args (dict): Extra args of S3 Service. Default {}
            additional args and kwargs


Download File
-------------

Download your file to an Bucket of AWS.

    .. code-block:: python

        from flask_cloud_services import storage_download_from_filename

        r = storage_download_from_filename(
            filename='/home/d2d/Desktop/test.txt',
            bucket_name='my-bucket-name',
            key='test.txt',
        )

    .. note:: Params

        .. code-block:: python

            filename (str): File path where you want to save it
            bucket_name (str): Bucket name
            key (str): File name in Bucket
            extra_args (dict): Extra args of S3 Service. Default {}
            additional args and kwargs


Get URL File
-------------

Get URL of your file located in Bucket of AWS.

    .. code-block:: python

        from flask_cloud_services import storage_get_file_url

        url = storage_get_file_url(
            bucket_name='my-bucket-name',
            key='my-file.pdf',
            *args,
            **kwargs
        )
        print(f"URL File: {url}")
    

    .. code-block:: python

        URL File: https://s3-us-west-2.amazonaws.com/my-bucket-name/my-file.pdf

    .. note:: Params

        .. code-block:: python

            bucket_name (str): Bucket name
            key (str): File name in Bucket
            additional args and kwargs
