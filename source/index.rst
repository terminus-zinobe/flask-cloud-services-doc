.. flask-cloud-services-doc documentation master file, created by
   sphinx-quickstart on Tue Jun 16 16:27:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flask Cloud Services's documentation!
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
