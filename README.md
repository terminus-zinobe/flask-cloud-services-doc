# Flask Cloud Services 

Package with Cloud Services on Flask.

## Environment
- Create
    ```shell
    $ python3 -m venv venv
    ```
- Activate
    ```shell
    $ source venv/bin/activate
    ```
- Deactivate
    ```shell
    $ deactivate
    ```

## Package installation
- Installation
    ```shell
    $ pip3 install flask-cloud-services
    ```

## HTML
- Create html
    ```shell
    $ make html
    ```

- View html
    ./build/html/index.html


## Refs:
- [Sphinx](http://www.sphinx-doc.org/en/master/)
- [Theme](https://sphinx-themes.org/html/sphinx-glpi-theme/glpi/index.html)
- [httpdomain](https://sphinxcontrib-httpdomain.readthedocs.io/en/stable/)